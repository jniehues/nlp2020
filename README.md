# README #

This is the repository  for the assignments in the NLP course at Maastricht University

## Content ##

* Assignment 1: Punctuation prediction

### Assignment 1 ###

Assignment 1 is about the task of predicting punctuation marks in an text that does not contain punctuation marks.

In this assignment you should use an language model to predict the marks. We will thereby limit ourselves to the
following marks : {,.!?}.

The provided code is in the subdirectory asgmt1/src.



