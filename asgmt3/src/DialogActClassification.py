
import sys, argparse
import json

import gensim.downloader as api

import keras
from keras.models import Sequential
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

def main():
    parser = argparse.ArgumentParser(description='Script to train a language model')
    parser.add_argument("--data", default="../data/MultiWOZ_2.1/data.json", type=str, help="json file containing data")
    parser.add_argument("--valid", default="../data/MultiWOZ_2.1/testListFile.txt", type=str, help="json file containing data")
    parser.add_argument("--test", default="../data/MultiWOZ_2.1/valListFile.txt", type=str, help="json file containing data")


    args = parser.parse_args()


    """ Load data and split into training and testing"""
    """ List of dialogs, each dialog has many turns"""
    train,valid,test = loadData(args.data,args.valid,args.test)


    """ Extract data for prediction: Single turn of human an label"""
    """Result: Tuple of two list: First sentence, second labels as strings"""
    train_acts = prepareData(train)
    valid_acts = prepareData(valid)
    test_acts = prepareData(test)




    """ Preprocess the labels"""
    labels = {}
    c = 0
    for l in set(train_acts[1]+valid_acts[1]+test_acts[1]):
        labels[l] = c
        c += 1

    class_size = c
    train_labels = keras.utils.to_categorical([labels[s] for s in train_acts[1]], num_classes=class_size)
    valid_labels = keras.utils.to_categorical([labels[s] for s in valid_acts[1]], num_classes=class_size)
    test_labels = keras.utils.to_categorical([labels[s] for s in test_acts[1]], num_classes=class_size)


    """ Load word embeddings and create tokenizer"""
    w2v_model = api.load("glove-wiki-gigaword-50")
    vocabulary = {word: vector.index for word, vector in w2v_model.vocab.items()}
    tk = Tokenizer(num_words=len(vocabulary))
    tk.word_index = vocabulary


    """ Preprocess input """
    train_input = pad_sequences(tk.texts_to_sequences(train_acts[0]))
    valid_input = pad_sequences(tk.texts_to_sequences(valid_acts[0]))
    test_input = pad_sequences(tk.texts_to_sequences(test_acts[0]))


    """ Create Model"""
    model = Sequential()


    emb = w2v_model.wv.get_keras_embedding(train_embeddings=False)
    model.add(emb)

    model.add(keras.layers.Lambda(lambda x: keras.backend.mean(x, axis=1)))
    model.add(keras.layers.Dense(class_size, activation='softmax'))

    model.summary()

    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    print('Training')
    model.fit(train_input, train_labels,
              batch_size=32,
              epochs=20,
              validation_data=(valid_input,valid_labels))

    print('Evaluation')
    loss, acc = model.evaluate(test_input, test_labels,
                               batch_size=32)
    print('Test loss / test accuracy = {:.4f} / {:.4f}'.format(loss, acc))

def prepareData(set):
    """the dialogs are stored in the "Log" dictonary entry"""
    input = []
    output = []
    for d in set:
        for i in range(len(d["log"])):

            """ Only user turns """
            if(i %2 == 0 or i %2 == 1):
                seg = d["log"][i]
                """ Only take turns by the human with annotated dialog_act"""
                if("dialog_act" in seg and len(list(seg["dialog_act"].keys())) > 0):
                    text = seg["text"]
                    input.append(text)
                    output.append(list(seg["dialog_act"].keys())[0])
    return (input,output)



def loadData(data,valid,test):

    testList =loadList(test)
    validList =loadList(valid)

    trainSet=[]
    testSet=[]
    validSet=[]

    with open(data) as json_file:
        dialogs = json.load(json_file)

    for k in dialogs.keys():
        if k in testList:
            testSet.append(dialogs[k])
        elif k in validList:
            validSet.append(dialogs[k])
        else:
            trainSet.append(dialogs[k])

    return trainSet,validSet,testSet

def loadList(filename):

    f = open(filename)
    l = f.readlines()
    return {s.strip():1 for s in l}

if __name__ == "__main__":
   main()
