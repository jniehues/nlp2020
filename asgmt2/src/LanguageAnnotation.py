class LanguageAnnotation:

    def checkSourceLanguage(self,string):
        """Check wether the word in string is from the source language"""
        return string[:2] == "s_"

    def checkTargetLanguage(self, string):
        """Check wether the word in string is from the target language"""
        return string[:2] == "t_"

    def annotateSourceWord(self,string):
        """Check wether the word in string is from the source language"""
        return "s_" + string
    def annotateTargetWord(self, string):
        """Check wether the word in string is from the target language"""
        return "t_" + string
