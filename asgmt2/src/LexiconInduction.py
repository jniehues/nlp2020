
import sys, argparse
from LanguageAnnotation import LanguageAnnotation
from Mapping import Mapping
from ShiftMapping import ShiftMapping
from LinearMapping import LinearMapping
from DataAugmentation import augment
import gensim


def main():
    parser = argparse.ArgumentParser(description='Script to train a language model')
    parser.add_argument("--source_corpus", default="../data/epps/EPPS.tiny.en", type=str, help="text file containing the source data")
    parser.add_argument("--target_corpus", default="../data/epps/EPPS.tiny.de", type=str, help="text file containing the source data")
    parser.add_argument("--dict", default="../data/wikititels/dict.train", type=str, help="text file containing the list of source words")
    parser.add_argument("--test_dict", default="../data/wikititels/dict.test", type=str, help="text file containing the list of source words")
    parser.add_argument("--model", default="word2vec", type=str, help="File containing the model")
    parser.add_argument("--load_model", default=False, type=bool, help="Load model")
    parser.add_argument("--mapping", default="No", type=str, help="Load model")
    parser.add_argument("--augment", default="No", type=str, help="Load model")

    args = parser.parse_args()

    ''' Annotate with langauge '''
    annot = LanguageAnnotation()

    if (args.load_model):
        model = gensim.models.Word2Vec.load(args.model)

        """ Read in seed dict"""
        seed_dict = [s.strip().split("#")[:2] for s in open(args.dict, encoding="utf-8").readlines()]
        seed_dict = [[annot.annotateSourceWord(t[0].strip()), annot.annotateTargetWord(t[1].strip())] for t in
                     seed_dict]


    else:
        ''' Load the corpus '''
        source = [s.strip().split() for s in open(args.source_corpus, encoding="utf-8").readlines()]
        target = [s.strip().split() for s in open(args.target_corpus, encoding="utf-8").readlines()]

        ''' Annotate with langauge '''

        source_data = []
        for line in source:
            source_data.append([annot.annotateSourceWord(w) for w in line])


        target_data = []
        for line in target:
            target_data.append([annot.annotateTargetWord(w) for w in line])
        data = source_data + target_data

        """ Read in seed dict"""
        seed_dict = [s.strip().split("#")[:2] for s in open(args.dict, encoding="utf-8").readlines()]
        seed_dict = [[annot.annotateSourceWord(t[0].strip()), annot.annotateTargetWord(t[1].strip())] for t in
                     seed_dict]

        """ Augment the data """
        if(args.augment == "Mixing"):
            print(len(data))
            data += augment(source_data,target_data,seed_dict)
            print(len(data))

        """ Train word 2 vec"""

        model = gensim.models.Word2Vec(
             data,
            size=150,
            window=10,
            min_count=2,
            workers=10,
            iter=10)
        model.save(args.model)


    """ Learn mapping"""
    if(args.mapping == "No"):
        mapping = Mapping(model,seed_dict)
    elif(args.mapping == "Shift"):
        mapping = ShiftMapping(model,seed_dict)
    elif(args.mapping == "Linear"):
        mapping = LinearMapping(model,seed_dict)


    """ Test model"""
    dict = [s.strip().split("#")[:2] for s in open(args.test_dict,encoding="utf-8").readlines()]
    dict = [[annot.annotateSourceWord(t[0].strip()),annot.annotateTargetWord(t[1].strip())] for t in dict]
    hitOne = 0
    hitThree = 0
    hitTen = 0
    count = 0
    for s,t in dict:
        if s in model.wv:
            """Number of target words seen so far"""
            pos = 1
            """ Position of the correct target word"""
            hit = -1
            for w, score in model.wv.similar_by_vector(mapping.map(s),
                                                           topn=100):
                if (w == t):
                    hit = pos
                    break;
                elif (annot.checkTargetLanguage(w)):
                    pos += 1
                    if (pos > 10):
                        break;
            count += 1
            if (hit > 0):
                if (hit <= 10):
                    hitTen += 1
                if (hit <= 3):
                    hitThree += 1
                if (hit <= 1):
                    hitOne += 1

    print("Found best match:", hitOne, " of ", count, " = ", 1.0 * hitOne / count)
    print("Found top 3 match:", hitThree, " of ", count, " = ", 1.0 * hitThree / count)
    print("Found top 10 match:", hitTen, " of ", count, " = ", 1.0 * hitTen / count)


if __name__ == "__main__":
   main()