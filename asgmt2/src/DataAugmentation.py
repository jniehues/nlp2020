def augment(source,target,dict):
    s_dict = {s[0].strip(): s[1].strip() for s in dict}
    t_dict = {s[1].strip(): s[0].strip() for s in dict}

    data = []
    for line in source:
        for i in range(len(line)):
            if line[i] in s_dict:
                l = line[:i] + [s_dict[line[i]]] + line[i + 1:]
                data.append(l)
    for line in target:
        for i in range(len(line)):
            if line[i] in t_dict:
                l = line[:i] + [t_dict[line[i]]] + line[i + 1:]
                data.append(l)
    return data
