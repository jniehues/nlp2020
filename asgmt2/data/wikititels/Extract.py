#!/usr/bin/python

import sys, argparse
from collections import defaultdict


def main():
    parser = argparse.ArgumentParser(description='Script to train a language model')
    parser.add_argument("--source_corpus", default="../epps/EPPS.en", type=str, help="text file containing the source data")
    parser.add_argument("--target_corpus", default="../epps/EPPS.de", type=str, help="text file containing the source data")
    parser.add_argument("--source_list", default="titels.txt.en", type=str, help="text file containing the list of source words")
    parser.add_argument("--target_list", default="titels.txt.de", type=str, help="text file containing the list of target words")
    parser.add_argument("--threshold", default=10, type=int, help="Minimun occurance")
    parser.add_argument("--output", default="dict", type=str, help="Outputfile")

    args = parser.parse_args()

    sCounts = getCounts(args.source_corpus)
    tCounts = getCounts(args.target_corpus)

    tf = open(args.target_list,encoding="utf-8")
    sf = open(args.source_list,encoding="utf-8")
    output = open(args.output,'w',encoding="utf-8")


    sl = sf.readline()
    tl = tf.readline()



    while(sl and tl):

        sw = sl.strip()
        tw = tl.strip()

        slw = sw[0].lower()+sw[1:]
        tlw = tw[0].lower()+tw[1:]
        if (sCounts[slw] > sCounts[sw]):
            sw = slw
        if (tCounts[tlw] > tCounts[tw]):
            tw = tlw

        if (sCounts[sw] > args.threshold and tCounts[tw] > args.threshold):
            output.write(sw+" # "+tw+" # "+str(sCounts[sw])+" "+str(tCounts[tw])+"\n")

        sl = sf.readline()
        tl = tf.readline()

    sf.close()
    tf.close()
    output.close()

def getCounts(filename):
    f = open(filename,encoding="utf-8")

    l = f.readline()

    frequency = defaultdict(int)
    while(l):

        words = l.strip().split()
        for w in words:
            frequency[w] += 1

        l = f.readline()

    f.close()
    return frequency


if __name__ == "__main__":
   main()